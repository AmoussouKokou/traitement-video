from flask import Flask, Response, request, render_template, redirect
from traitement import Traitement

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

    """
    The function `action()` in a Python Flask app handles different actions based on the form input and
    redirects to corresponding routes.
    :return: In the provided code snippet, if the value of the variable `action` is 'mp4_to_mp3', the
    code snippet does not specify what should be returned. You can add the appropriate return statement
    based on the desired behavior for the 'mp4_to_mp3' action. For example, you can redirect to a
    specific route or return a response based on the action.
    """
@app.route('/action', methods=['POST'])
def action():
    action = request.form['action']
    if action == 'yt_to_mp4':
        return redirect('/yt_to_mp4')
    elif action == 'yt_to_mp3':
        return redirect('/yt_to_mp3')
    elif action == 'mp4_to_mp3':
        return redirect('/mp4_to_mp3')
    elif action == 'couper_mp5':
        return redirect('/couper_mp3')
    else:
        return "Action non reconnue"

@app.route('/yt_to_mp4', methods=['POST', 'GET'])
def yt_to_mp4():
    if request.method == 'POST':
        lien_youtube = request.form['lien_youtube']
        output = request.form['output']
        trt = Traitement(lien_youtube)
        # trt.telecharger_yt_to_mp4(output)
        # return "Conversion réussie !"  # Vous pouvez rediriger l'utilisateur ou afficher un message de succès ici
        # Utilisation de yield pour envoyer progressivement les informations de progression du téléchargement via SSE
        def generate():
            yield "Téléchargement en cours...\n\n"
            trt.telecharger_yt_to_mp4(output)
            yield "Téléchargement réussie !\n\n"
        
        return Response(generate(), content_type='text/event-stream')
    else:
        return render_template('yt_to_mp4.html')

@app.route('/yt_to_mp3')
def yt_to_mp3():
    # Code pour convertir YouTube en MP3
    return "Convertir YouTube en MP3"

@app.route('/mp4_to_mp3')
def mp4_to_mp3():
    # Code pour convertir MP4 en MP3
    return "Convertir MP4 en MP3"

@app.route('/couper_mp3')
def couper_mp3():
    # Code pour couper MP3
    return "Couper MP3"

if __name__ == '__main__':
    app.run(debug=True)



# -----------------------
# '''
# from flask import Flask, request, render_template
# app = Flask(__name__)


# @app.route('/')
# def index():
#     return render_template('index.html')

# @app.route('/convert', methods=['POST'])
# def convert():
#     if request.method == 'POST':
#         lien_youtube = request.form['lien_youtube']
#         chemin_image = 'save/img.png'  # Chemin de l'image de fond
#         traitement = Traitement(lien_youtube)
#         traitement.telecharger_yt_to_mp4("save")
#         traitement.mp4_to_mp3()
#         traitement.convert_mp3_to_mp4_with_bg(chemin_image)
#         # Vous pouvez rediriger l'utilisateur vers une page de téléchargement ou afficher un message de succès ici
#         return "Conversion réussie !"

# if __name__ == '__main__':
#     app.run(debug=True)
# '''


