import streamlit as st
from pytube import YouTube
import OPTIONS as op

# Fonction pour télécharger la vidéo depuis YouTube
@st.cache_data
@st.cache_resource
def download_youtube_video(video_url):
    yt = YouTube(video_url)
    stream = yt.streams.get_highest_resolution()
    if stream:
        video_title = yt.title
        temp_video_path = f"{video_title}.mp4"
        stream.download(filename=temp_video_path)
        return video_title, temp_video_path
    else:
        st.error("Impossible de trouver une résolution pour la vidéo.")
        return None, None
    
def output_options(input):
    if input == "youtube":
        return ["mp4"]
    else:
        return op.TYPE