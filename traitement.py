import os
# from pytube import YouTube
from moviepy.editor import *
from pydub import AudioSegment
import yt_dlp
import sys
sys.path.append("./")

class Traitement:
    def __init__(self, chemin_fichier):
        """Cette classe permet de traiter un fichier mp3 ou mp4.

        Args:
            chemin_fichier (str): Chemin jusqu'au fichier. Peut être un lien YouTube.
        """
        
        self.chemin = None
        self.type = None
        self.nom = None
        self.lien = None
        split_fichier = chemin_fichier.split("/")
        if split_fichier[-1].split(".")[-1] not in ("mp3", "mp4"):
            print("Lien YouTube détecté !!!")
            self.type = "yt"
            self.lien = chemin_fichier
        
        if split_fichier[-1].split(".")[-1] == "mp3":
            self.type = "mp3"
            self.chemin = os.path.dirname(chemin_fichier)
            self.nom = os.path.basename(chemin_fichier)
        
        if split_fichier[-1].split(".")[-1] == "mp4":
            self.type = "mp4"
            self.chemin = os.path.dirname(chemin_fichier)
            self.nom = os.path.basename(chemin_fichier)
    
    
    def est_lien_youtube_valide(self, lien):
        import re
        regex = r'(https?://)?(www\.)?(youtube|youtu|youtube-nocookie)\.(com|be)/(watch\?v=|embed/|v/|.+\?v=)?([^&=%\?]{11})'
        return re.match(regex, lien) is not None

    def telecharger_yt_to_mp4(self, chemin_sortie_yt_to_mp4):
        """Télécharge une vidéo YouTube au format mp4.

        Args:
            chemin_sortie_yt_to_mp4 (str): Chemin de sortie pour le fichier téléchargé.
        """

        if self.type != "yt":
            raise Exception(self.chemin + " n'est pas un lien YouTube")

        if not self.est_lien_youtube_valide(self.lien):
            raise Exception(f"lien non valide ({self.lien})")

        ydl_opts = {
            'format': 'bestvideo+bestaudio/best',
            'outtmpl': f'{chemin_sortie_yt_to_mp4}/%(title)s.%(ext)s',
            'merge_output_format': 'mp4',
        }

        try:
            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                info_dict = ydl.extract_info(self.lien, download=True)
                video_title = info_dict.get('title', None)
                print(f"Téléchargement terminé !!! {chemin_sortie_yt_to_mp4}")
        except Exception as e:
            raise Exception("Erreur lors du téléchargement : " + str(e))

        self.chemin = chemin_sortie_yt_to_mp4
        self.nom = f"{video_title}.mp4"
        return self
    # def telecharger_yt_to_mp4(self, chemin_sortie_yt_to_mp4):
    #     """Télécharge une vidéo YouTube au format mp4.

    #     Args:
    #         chemin_sortie_yt_to_mp4 (str): Chemin de sortie pour le fichier téléchargé.
    #     """
        
    #     if self.type != "yt":
    #         raise Exception(self.chemin + " n'est pas un lien YouTube")
        
    #     if not self.est_lien_youtube_valide(lien):
    #         raise Exception(f"lien non valide ({self.lien})")
        
    #     video = YouTube(self.lien)
    #     stream = video.streams.get_highest_resolution()
    #     if stream:
    #         stream.download(output_path=chemin_sortie_yt_to_mp4)
    #         print(f"Téléchargement terminé !!! {chemin_sortie_yt_to_mp4}")
    #     else:
    #         raise Exception("Aucun flux valide trouvé !")
            
    #     self.chemin = chemin_sortie_yt_to_mp4
    #     self.nom = stream.default_filename
    #     return self
            
    def mp4_to_mp3(self, chemin_sortie_mp4_to_mp3=None, skip_if_exists=False):
        """Convertit un fichier mp4 en mp3.

        Args:
            chemin_sortie_mp4_to_mp3 (str, optional): Chemin de sortie pour le fichier mp3 converti. 
                Par défaut, le fichier sera sauvegardé dans le même dossier que le fichier d'entrée.

        Returns:
            Traitement: Instance de la classe Traitement.
        """
        path = os.path.join(self.chemin, self.nom)
        # if self.output_yt_to_mp4 is not None:
        #     path = self.output_yt_to_mp4
        # else:
        #     if self.nom.split(".")[-1] == "mp4":
        #         path = self.chemin + "/" + self.nom
        #     else:
        #         raise Exception("Impossible de convertir un fichier " + self.nom.split(".")[-1] + " en mp3")

        if chemin_sortie_mp4_to_mp3 is None:
            output = path.replace(".mp4", ".mp3")
        elif chemin_sortie_mp4_to_mp3.split(".")[-1] != "mp3":
            p = path.replace('.mp4', '.mp3')
            print(f"Chemin non valide, sauvegarde dans {p}")
            output = path.replace(".mp4", ".mp3")
        else:
            output = chemin_sortie_mp4_to_mp3
        
        if skip_if_exists and os.path.exists(output):                  #!!!!!!
            self.chemin = os.path.dirname(output)                      #!!!!!!
            self.nom = os.path.basename(output)                        #!!!!!!
            return f"{output} existe, skip."                           #!!!!!!
        
        clip = VideoFileClip(path)
        clip.audio.write_audiofile(output)
        
        self.chemin = os.path.dirname(output)
        self.nom = os.path.basename(output)
        return self
    
    
    def couper_mp3(self, debut=None, fin=None, chemin_sortie_couper_mp3=None, skip_if_exists=False):
        """Coupe un fichier audio mp3.

        Args:
            debut (int, optional): Temps de début de la coupe en millisecondes. Par défaut, None.
            fin (int, optional): Temps de fin de la coupe en millisecondes. Par défaut, None.
            chemin_sortie_couper_mp3 (str, optional): Chemin de sortie pour le fichier audio coupé. 
                Par défaut, le fichier sera sauvegardé dans le même dossier que le fichier d'entrée avec un suffixe '_coupe.mp3'.

        Returns:
            Traitement: Instance de la classe Traitement.
        """
        debut = self.hour_to_min(hh_mm_ss=debut)
        fin = self.hour_to_min(hh_mm_ss=fin)
        
        if fin is not None: 
            fin = fin*1000*60
        if debut is not None:
            debut = debut*1000*60
        
        print(debut, fin)
        chemin_fichier = os.path.join(self.chemin, self.nom)
        # print(os.path.exists(chemin_fichier))
        print("Coupe de :", chemin_fichier)
        
        if not os.path.exists(chemin_fichier):
            print(f"Le fichier {chemin_fichier} n'existe pas.")
            return
        
        # Déterminer le chemin de sortie
        if chemin_sortie_couper_mp3 is None:
            chemin_sortie_couper_mp3 = chemin_fichier.replace('.mp3', '_coupe.mp3')
            # print(f"chemin {chemin_sortie_couper_mp3} non trouvé. Sauvegarde dans {chemin_fichier.replace('.mp3', '_coupe.mp3')}")
        
        if skip_if_exists and os.path.exists(chemin_sortie_couper_mp3):                  #!!!!!!
            self.chemin = os.path.dirname(chemin_sortie_couper_mp3)                      #!!!!!!
            self.nom = os.path.basename(chemin_sortie_couper_mp3)                        #!!!!!!
            return f"{chemin_sortie_couper_mp3} existe, skip."                           #!!!!!!
        
        # Charger le fichier audio MP3
        audio = AudioSegment.from_mp3(chemin_fichier)

        # Si début et fin sont None, retourner une copie du fichier audio original
        if debut is None and fin is None:
            return chemin_fichier

        # Si début est None, couper du début jusqu'à la fin spécifiée
        if debut is None:
            audio_coupe = audio[:fin]
        else:
            # Si fin est None, couper de début jusqu'à la fin de l'audio
            if fin is None:
                audio_coupe = audio[debut:]
            else:
                # Couper de début jusqu'à la fin spécifiée
                audio_coupe = audio[debut:fin]

        
        # Enregistrer le fichier coupé
        audio_coupe.export(chemin_sortie_couper_mp3, format="mp3")

        print(chemin_sortie_couper_mp3)
        
        self.chemin = os.path.dirname(chemin_sortie_couper_mp3)
        self.nom = os.path.basename(chemin_sortie_couper_mp3)
        return self

    def joindre_mp3(self, *chemins_audios, chemin_sortie_joindre_mp3=None, skip_if_exists=False):
        """Joindre des fichiers audio mp3.

        Args:
            *chemins_audios (str): Chemins des fichiers audio à joindre.
            chemin_sortie_joindre_mp3 (str, optional): Chemin de sortie pour le fichier audio joint. 
                Par défaut, le fichier sera sauvegardé dans le même dossier que le fichier d'entrée avec un suffixe '_joints.mp3'.

        Returns:
            Traitement: Instance de la classe Traitement.
        """
        
        if self.nom.split(".")[-1] != "mp3":
            raise ValueError("Le fichier doit être au format MP3.")
        
        chemin_fichier = os.path.join(self.chemin, self.nom)
        # Vérifier s'il y a au moins deux chemins audio
        if len(chemins_audios) < 1:
            raise ValueError("Vous devez fournir au moins 1 chemin audio pour le joindre avec le fichier actuel.")
        
        # Déterminer le chemin de sortie
        if chemin_sortie_joindre_mp3 is None:
            chemin_sortie_joindre_mp3 = chemin_fichier.replace('.mp3', '_joints.mp3')
        
        if skip_if_exists and os.path.exists(chemin_sortie_joindre_mp3):                  #!!!!!!
            self.chemin = os.path.dirname(chemin_sortie_joindre_mp3)                      #!!!!!!
            self.nom = os.path.basename(chemin_sortie_joindre_mp3)                        #!!!!!!
            return f"{chemin_sortie_joindre_mp3} existe, skip."                           #!!!!!!
        
        # Charger le premier fichier audio
        audio_joint = AudioSegment.from_mp3(chemin_fichier)
        
        # Joindre les fichiers audio restants
        for chemin_audio in chemins_audios:
            audio_joint += AudioSegment.from_mp3(chemin_audio)
        
        # Exporter le fichier audio joint
        audio_joint.export(chemin_sortie_joindre_mp3, format="mp3")
        
        print("Fichier audio joint enregistré sous :", chemin_sortie_joindre_mp3)
        
        self.chemin = os.path.dirname(chemin_sortie_joindre_mp3)
        self.nom = os.path.basename(chemin_sortie_joindre_mp3)
        return self


    def convert_mp3_to_mp4_with_bg(self, image_path, chemin_sortie_cv_mp3_to_mp4=None, fps=24, skip_if_exists=False):
        """Convertit un fichier audio mp3 en une vidéo mp4 avec une image de fond.

        Args:
            image_path (str): Chemin de l'image de fond pour la vidéo.
            chemin_sortie_cv_mp3_to_mp4 (str, optional): Chemin de sortie pour la vidéo mp4 convertie. 
                Par défaut, le fichier sera sauvegardé dans le même dossier que le fichier audio d'entrée avec un suffixe '_cv.mp4'.
            fps (int, optional): Nombre d'images par seconde pour la vidéo. Par défaut, 24.

        Returns:
            Traitement: Instance de la classe Traitement.
        """
        
        if self.nom.split(".")[-1] != "mp3":
            raise ValueError("Le fichier doit être au format MP3.")
        
        if skip_if_exists and os.path.exists(chemin_sortie_cv_mp3_to_mp4):                  #!!!!!!
            self.chemin = os.path.dirname(chemin_sortie_cv_mp3_to_mp4)                      #!!!!!!
            self.nom = os.path.basename(chemin_sortie_cv_mp3_to_mp4)                        #!!!!!!
            return f"{chemin_sortie_cv_mp3_to_mp4} existe, skip."                           #!!!!!!
        
        audio_path = os.path.join(self.chemin, self.nom)
        
        # Charger l'audio
        audio = AudioFileClip(audio_path)

        # Charger l'image de fond
        background = ImageClip(image_path)

        # Définir la durée de la vidéo comme étant la durée de l'audio
        duration = audio.duration

        # Créer une vidéo avec l'image de fond et l'audio en spécifiant la durée
        video = background.set_audio(audio).set_duration(duration)
        
        if chemin_sortie_cv_mp3_to_mp4 is None:
            chemin_sortie_cv_mp3_to_mp4 = audio_path.replace('.mp3', '_cv.mp4')
            
        # Écrire la vidéo au format MP4 en spécifiant la valeur de fps
        video.write_videofile(chemin_sortie_cv_mp3_to_mp4, codec="libx264", audio_codec="aac", fps=fps)
        
        self.chemin = os.path.dirname(chemin_sortie_cv_mp3_to_mp4)
        self.nom = os.path.basename(chemin_sortie_cv_mp3_to_mp4)
        return self
    
    @staticmethod
    def hour_to_min(hh_mm_ss: str|None):
        """ex: hour=1:10:30.5"""
        if hh_mm_ss is None:
            return hh_mm_ss
        
        if ":" not in hh_mm_ss:
            try:
                ss=float(hh_mm_ss)
                return ss/60
            except Exception as e:
                raise("Non valide !!!")
        
        hh_mm_ss_l = hh_mm_ss.split(":")
        
        if len(hh_mm_ss_l)<2:
            hh_mm_ss_l.insert(0, "00")
        
        if len(hh_mm_ss_l)<3:
            hh_mm_ss_l.insert(0, "00")
        
        hh_mm_ss_l_cv = list(map(float, hh_mm_ss_l))
        
        # if (hh_mm_ss_l_cv[0] >= 24) or (hh_mm_ss_l_cv[1] >= 60) or (hh_mm_ss_l_cv[2] >= 60):
        #     raise(f"Heure non valide {hh_mm_ss_l_cv[0]}h{hh_mm_ss_l_cv[1]}m{hh_mm_ss_l_cv[2]}s")
        
        return (hh_mm_ss_l_cv[0]*60 + hh_mm_ss_l_cv[1] + hh_mm_ss_l_cv[2]/60)
    
if __name__ == "__main__":
    lien = "https://www.youtube.com/watch?v=yHRi9Pea26Y"
    # lien = "https://www.youtube.com/watch?app=desktop&v=6ADsF-N6yO0"
    # lien = "https://www.youtube.com/watch?v=Grzq1-B3ZpY&t=1747s"
    # for lien in liens:
    # Traitement(lien).telecharger_yt_to_mp4("save/SEnracinerEnChrist2").mp4_to_mp3()
    chemin_fichier="D:/PAROLE/traitement-video/"
    # Traitement(chemin_fichier)\
    #     .joindre_mp3("17-11-/audio 2.mp3")
    
    Traitement(lien).telecharger_yt_to_mp4("Chants/").mp4_to_mp3()
    # Traitement(f"{chemin_fichier}Partage.mp3")\
        # .mp4_to_mp3()\
        # .couper_mp3(debut="1:13:35", fin=None)
        # .convert_mp3_to_mp4_with_bg(chemin_fichier+"image.png")
        
    
    # chemin_fichier="Chants/Yehoshoua ma vie.mp4"
    # Traitement(chemin_fichier)\
    #     .mp4_to_mp3()
    # cvmin= 1000*60
    # chemin_fichier="save/partage/"
    # Traitement(chemin_fichier+"Festin.mp3")\
    #     .couper_mp3(
    #         debut=None, 
    #         fin=(21.5/60)*cvmin, 
    #         chemin_sortie_couper_mp3=chemin_fichier+"Festin_court.mp3"
    #     )
    
    # cvmin= 1000*60
    # chemin_fichier="save/veiller_prier/"
    # Traitement(chemin_fichier+"NePasSinquieter.mp3")\
    #     .couper_mp3(
    #         debut=1.716*cvmin, 
    #         # fin=120*cvmin, 
    #         chemin_sortie_couper_mp3=chemin_fichier+"nouveau/NePasSinquieter.mp3"
    #     )\
    #     .convert_mp3_to_mp4_with_bg(
    #         image_path=chemin_fichier+"image_1.png",
    #         chemin_sortie_cv_mp3_to_mp4=chemin_fichier+"nouveau/NePasSinquieter.mp4"
    #     )
    
    # Traitement(chemin_fichier+"ChacunASonRoleAJouer.mp3")\
    #     .convert_mp3_to_mp4_with_bg(
    #         image_path=chemin_fichier+"image_2.png",
    #         chemin_sortie_cv_mp3_to_mp4=chemin_fichier+"nouveau/ChacunASonRoleAJouer.mp4"
    #     )
        
    # Traitement(chemin_fichier+"Veiller et Prier.mp3")\
    #     .couper_mp3(
    #         debut=38.233*cvmin, 
    #         fin=83.4*cvmin, 
    #         chemin_sortie_couper_mp3=chemin_fichier+"NePasSinquieter.mp3", skip_if_exists=True
    #     )\
    #     .joindre_mp3(chemin_fichier+"Festin_court.mp3", skip_if_exists=True)\
    #     .convert_mp3_to_mp4_with_bg(
    #         image_path=chemin_fichier+"image_1.png",
    #         chemin_sortie_cv_mp3_to_mp4=chemin_fichier+"NePasSinquieter.mp4", skip_if_exists=True
    #     )
    
    
    # Traitement(chemin_fichier+"NePasSinquieter.mp3")\
    #     .couper_mp3(
    #         debut=1.716*cvmin, 
    #         # fin=120*cvmin, 
    #         chemin_sortie_couper_mp3=chemin_fichier+"ChacunASonRoleAJouer.mp3"
    #     )\
    #     .joindre_mp3(chemin_fichier+"Festin_court.mp3", chemin_sortie_joindre_mp3=chemin_fichier+"ChacunASonRoleAJouer_jf.mp3")
    
    # Traitement(chemin_fichier+"Festin_court.mp3")\
    #     .joindre_mp3(chemin_fichier+"ChacunASonRoleAJouer_jf.mp3", chemin_sortie_joindre_mp3=chemin_fichier+"ChacunASonRoleAJouer_jff.mp3")\
    #     .convert_mp3_to_mp4_with_bg(
    #         image_path=chemin_fichier+"image_2.png",
    #         chemin_sortie_cv_mp3_to_mp4=chemin_fichier+"ChacunASonRoleAJouer.mp4"
    #     )
        
    
    # liens = ["https://www.youtube.com/watch?v=kM-eFDIsD_I"]
    
    # liens = ["https://youtu.be/Qg2to6eq_fI?si=YXZoWu9jZeuzy7mF",
    #         "https://youtu.be/46Wk2Qo4UCg?si=h1SLKNYIiPx4ht69",
    #         "https://youtu.be/RR5n-3jBlXU?si=sTt3yQKffBnWqNuD",
    #         "https://www.youtube.com/live/2teLXkwjzG4?si=2fSfv93FbA9Gni2q",
    #         "https://www.youtube.com/live/Lj1wBsdvN0M?si=J_5SaV8ChD_AuGYd",
    #         "https://youtu.be/Qg2to6eq_fI?si=3F54to15Oj3pKebt",
    #         "https://www.youtube.com/live/oCG5DRz6SSU?si=8_nX0LaF_Qve-eHY",
    #         "https://youtu.be/efuiQOfDQc8?si=GjYa1Y4mU80S3hUD",
    #         "https://www.youtube.com/live/IwwM1YQX_Zc?si=YOrmE1PNrKRCiKU8",
    #         "https://www.youtube.com/live/VmQmKpf0dzo?si=-PiNy5y89XsVol3g",
    #         "https://www.youtube.com/live/BY_wsJulWog?si=Dyt8hdg10Cixwj6I",
    #         "https://www.youtube.com/live/BY_wsJulWog?si=B2f1iHb5ptgOWIL6",
    #         "https://youtu.be/qs6Q2s1Z6o4?si=TMokXeRMbf3nu8QV",
    #         "https://youtu.be/Gpr0iCuY4Dg?si=xhzTXyW8ToTNCR8p",
    #         "https://www.youtube.com/live/6nPTzVEp_S4?si=MaaEq3wBeQrkfIVs",
    #         "https://youtu.be/TMk6SnCDIsY?si=9pTiXlWLsRpj3U6W",
    #         "https://www.youtube.com/live/CtUHATI-oZU?si=sQpsmV4lrY9GTKLV",
    #         "https://youtu.be/mIU2mbqte0g?si=W1pjQOkIFPOXwbL_",
    #         "https://youtu.be/QFmgAL7Q6Ac?si=UNDfOgjSZAMLIUsk",
    #         "https://youtu.be/6ADsF-N6yO0?si=VQTOZ-IHuSf8sXbH",
    #         ]
    
    # for lien in liens:
        # Traitement(lien).telecharger_yt_to_mp4("save")
        # .mp4_to_mp3()\
        # .convert_mp3_to_mp4_with_bg("save/img.png")

    # print(os.path.exists("save/veiller_prier/Veiller_et_Prier.mp3")) # "save/veiller_prier/Veiller et Prier.mp3"
    # f = Traitement("https://www.youtube.com/watch?v=kM-eFDIsD_I")\
    #     .telecharger_yt_to_mp4("save/veiller_prier/")\
    #     .mp4_to_mp3()\
    #     .couper_mp3(debut=38.14*cvmin, fin=38.14*46.21*cvmin)\
    #     .convert_mp3_to_mp4_with_bg("save/veiller_prier/image_1.png")
    
    # f2 = Traitement("https://www.youtube.com/watch?v=kM-eFDIsD_I")\
    #     .telecharger_yt_to_mp4("save/veiller_prier/")\
    #     .mp4_to_mp3()\
    #     .couper_mp3(debut=38.14*46.21*cvmin, fin=38.14*46.21*cvmin)\
    #     .convert_mp3_to_mp4_with_bg("save/veiller_prier/image_2.png")
