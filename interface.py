import os
from tkinter import *
from tkinter import filedialog, messagebox
from pytube import YouTube
from moviepy.editor import *
from pydub import AudioSegment
import sys
sys.path.append("./")
from traitement import Traitement

class Application:
    def __init__(self, root):
        self.root = root
        self.root.title("Éditeur de Vidéo")

        # Boutons pour les fonctionnalités
        Button(root, text="Importer MP3/MP4", command=self.importer_fichier).pack(pady=10)
        Button(root, text="Télécharger depuis YouTube", command=self.telecharger_youtube).pack(pady=10)
        Button(root, text="Convertir MP4 en MP3", command=self.convertir_mp4_en_mp3).pack(pady=10)
        Button(root, text="Couper MP3", command=self.couper_mp3).pack(pady=10)
        Button(root, text="Joindre MP3", command=self.joindre_mp3).pack(pady=10)
        Button(root, text="Ajouter une image à MP3", command=self.ajouter_image_mp3).pack(pady=10)

        self.chemin_fichier = None

    def importer_fichier(self):
        self.chemin_fichier = filedialog.askopenfilename(filetypes=[("MP3/MP4 Files", "*.mp3 *.mp4")])
        if self.chemin_fichier:
            messagebox.showinfo("Fichier Importé", f"Fichier importé: {self.chemin_fichier}")

    def telecharger_youtube(self):
        lien = filedialog.askstring("Télécharger depuis YouTube", "Entrez le lien YouTube:")
        if lien:
            chemin_sortie = filedialog.askdirectory()
            traitement = Traitement(lien)
            traitement.telecharger_yt_to_mp4(chemin_sortie)
            messagebox.showinfo("Téléchargement Terminé", f"Vidéo téléchargée à: {chemin_sortie}")

    def convertir_mp4_en_mp3(self):
        if not self.chemin_fichier or not self.chemin_fichier.endswith(".mp4"):
            messagebox.showerror("Erreur", "Veuillez importer un fichier MP4.")
            return
        traitement = Traitement(self.chemin_fichier)
        traitement.mp4_to_mp3()
        messagebox.showinfo("Conversion Terminée", f"Fichier converti en MP3.")

    def couper_mp3(self):
        if not self.chemin_fichier or not self.chemin_fichier.endswith(".mp3"):
            messagebox.showerror("Erreur", "Veuillez importer un fichier MP3.")
            return
        debut = filedialog.askstring("Couper MP3", "Entrez le temps de début en millisecondes:")
        fin = filedialog.askstring("Couper MP3", "Entrez le temps de fin en millisecondes:")
        traitement = Traitement(self.chemin_fichier)
        traitement.couper_mp3(int(debut), int(fin))
        messagebox.showinfo("Coupe Terminée", "Fichier MP3 coupé.")

    def joindre_mp3(self):
        fichiers = filedialog.askopenfilenames(filetypes=[("MP3 Files", "*.mp3")])
        if len(fichiers) < 2:
            messagebox.showerror("Erreur", "Veuillez sélectionner au moins deux fichiers MP3.")
            return
        traitement = Traitement(fichiers[0])
        traitement.joindre_mp3(*fichiers[1:])
        messagebox.showinfo("Jonction Terminée", "Fichiers MP3 joints.")

    def ajouter_image_mp3(self):
        if not self.chemin_fichier or not self.chemin_fichier.endswith(".mp3"):
            messagebox.showerror("Erreur", "Veuillez importer un fichier MP3.")
            return
        image_path = filedialog.askopenfilename(filetypes=[("Image Files", "*.png *.jpg *.jpeg")])
        traitement = Traitement(self.chemin_fichier)
        traitement.convert_mp3_to_mp4_with_bg(image_path)
        messagebox.showinfo("Conversion Terminée", "Fichier MP3 converti en MP4 avec image.")

if __name__ == "__main__":
    root = Tk()
    app = Application(root)
    root.mainloop()
