import os
import streamlit as st
from pytube import YouTube
from moviepy.editor import *
from pydub import AudioSegment

class Traitement:
    def __init__(self, lien):
        self.lien = lien
        self.chemin = ""
        self.nom = ""

    def telecharger_yt_to_mp4(self, chemin_sortie="save/"):
        yt = YouTube(self.lien)
        stream = yt.streams.filter(file_extension="mp4").first()
        self.nom = f"{yt.title}.mp4"
        self.chemin = chemin_sortie
        stream.download(output_path=self.chemin, filename=self.nom)
        return self

    def mp4_to_mp3(self):
        mp4_path = os.path.join(self.chemin, self.nom)
        mp3_path = mp4_path.replace(".mp4", ".mp3")
        audio_clip = AudioFileClip(mp4_path)
        audio_clip.write_audiofile(mp3_path)
        audio_clip.close()
        os.remove(mp4_path)
        self.nom = mp3_path
        return self

    def couper_mp3(self, debut=0, fin=10000):
        mp3_path = os.path.join(self.chemin, self.nom)
        audio = AudioSegment.from_mp3(mp3_path)
        audio = audio[debut:fin]
        audio.export(mp3_path, format="mp3")
        return self

    def convert_mp3_to_mp4_with_bg(self, image_path="image.png", fps=24):
        mp3_path = os.path.join(self.chemin, self.nom)
        mp4_path = mp3_path.replace(".mp3", ".mp4")
        audio_clip = AudioFileClip(mp3_path)
        image_clip = ImageClip(image_path).set_duration(audio_clip.duration).set_fps(fps)
        video = image_clip.set_audio(audio_clip)
        video.write_videofile(mp4_path, fps=fps)
        os.remove(mp3_path)
        self.nom = mp4_path
        return self

def main():
    st.title("Téléchargeur et Convertisseur YouTube")

    lien = st.text_input("Entrez le lien YouTube", "")
    chemin_sortie = st.text_input("Chemin de téléchargement", "save/")

    if st.button("Télécharger et Convertir"):
        if lien:
            try:
                traitement = Traitement(lien).telecharger_yt_to_mp4(chemin_sortie)
                traitement.mp4_to_mp3()
                st.success(f"Fichier MP3 sauvegardé dans {traitement.chemin}")
                st.audio(os.path.join(traitement.chemin, traitement.nom))
            except Exception as e:
                st.error(f"Erreur : {e}")
        else:
            st.error("Veuillez entrer un lien YouTube valide.")

    st.subheader("Couper le fichier MP3")
    debut = st.number_input("Début (en millisecondes)", value=0)
    fin = st.number_input("Fin (en millisecondes)", value=10000)
    if st.button("Couper le MP3"):
        try:
            traitement.couper_mp3(debut=debut, fin=fin)
            st.success(f"Fichier MP3 coupé sauvegardé dans {traitement.chemin}")
            st.audio(os.path.join(traitement.chemin, traitement.nom))
        except Exception as e:
            st.error(f"Erreur : {e}")

    st.subheader("Convertir MP3 en MP4 avec une image de fond")
    image_path = st.text_input("Chemin de l'image de fond", "image.png")
    fps = st.number_input("FPS", value=24)
    if st.button("Convertir en MP4"):
        try:
            traitement.convert_mp3_to_mp4_with_bg(image_path, fps=fps)
            st.success(f"Vidéo MP4 sauvegardée dans {traitement.chemin}")
            st.video(os.path.join(traitement.chemin, traitement.nom))
        except Exception as e:
            st.error(f"Erreur : {e}")

if __name__ == "__main__":
    main()
