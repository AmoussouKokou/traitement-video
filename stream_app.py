import streamlit as st
import os
from pytube import YouTube
import OPTIONS as op
import fonctions as ft
import base64
from moviepy.editor import VideoFileClip

# https://youtu.be/UDtRs1H61yc?si=9I1lTMp3JJZtr-Gf

st.header("Convertisseur de fichier")

a, b, c = st.columns([0.2, 0.6, 0.2])

input = a.selectbox("Input", options=op.TYPE, index=None)
if input:
    if input == "youtube":
        path = b.text_input("Entrer l'url de la vidéo")
        if path:
            # Télécharger la vidéo
            video_title, temp_video_path = ft.download_youtube_video(path)
            if a.checkbox("Visualiser"):
                b.write(video_title)
                b.video(temp_video_path)
                
    elif input == "mp4":
        path = b.file_uploader("Charger le fichier mp4", type=["mp4"])
        if path is not None and a.checkbox("Visualiser"):
            video_bytes = path.read()
            b.video(video_bytes)
            
    elif input == "mp3":
        path = b.file_uploader("Charger le fichier mp3", type=["mp3"])
        if path is not None and a.checkbox("Ecouter"):
            audio_bytes = path.read()
            b.audio(audio_bytes, format="audio/mpeg")

output = c.selectbox("Output", options=ft.output_options(input), index=None)
if output == "mp4":
    if path:
        # Bouton de téléchargement
        with open(temp_video_path, "rb") as video_file:
            video_bytes = video_file.read()
            b64 = base64.b64encode(video_bytes).decode()
            c.markdown(f'<a href="data:video/mp4;base64,{b64}" download="{video_title}.mp4">Télécharger</a>', 
                        unsafe_allow_html=True)